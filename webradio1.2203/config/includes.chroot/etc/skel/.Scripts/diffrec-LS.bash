#!/bin/bash

# variables générales: fichiers qui stockent les réglages et test de l'existence d'une IP
FICHIERCS='.Scripts/config/cs'
FICHIERPM='.Scripts/config/pm'
FICHIERQDIFF='.Scripts/config/qdiff'
FICHIERQREC='.Scripts/config/qrec'
IP=$(hostname -I | awk '{ print $1}')
# variables liquidsoap
SERVEURLOCAL='localhost'
PORTICECAST='8000'
PMLOCAL='webradio.ogg'
PASSLOCAL='webradio'
PMACAD=$(cat $FICHIERPM | grep ^pm | cut -d"," -f2)
PASSACAD=$(cat $FICHIERPM | grep ^pass | cut -d"," -f2)
SERVEURACAD='webradio.ac-versailles.fr'
PMAIRTIME='webradio.ogg'
PASSAIRTIME='webradio'
PORTAIRTIME='8001'
USERAIRTIME='source'
PMMONITOR='monitor.ogg'
PASSMONITOR='monitor'
REPREC='Enregistrements'
FICHIERREC='%Y-%m-%d-%H_%M_%S.ogg'
VLCVIEW='--zoom 0.5 --audio-visual visual --effect-list spectrum'
TEXTEZENREC="Répertoire d'enregistrement: '$REPREC' (fichier .ogg horodaté)."
TEXTEZENDIFFLOCAL="Adresse de diffusion: http://$IP:$PORTICECAST/$PMLOCAL"
TEXTEZENDIFFINT="Adresse de diffusion: http://$SERVEURACAD/$PMACAD"
TEXTEZENAIR="Flux envoyé vers la 'Source Maître' d'Airtime"
TEXTEMONITOR="Souhaitez-vous monitorer le flux avec VLC?"

function annulzen {
# possibilité d'annuler la config en cliquant sur annuler
if [ "$?" -eq 1 ]; then
    exit
fi
}

function configPM {
CFPM=$(zenity --width=500 --forms \
    --title="Configuration du point de diffusion" \
    --text="Définition du point de diffusion et du mot de passe" \
    --add-entry="Nom du point de diffusion (sous la forme etab-type-ville.mp3 ou etab-type-ville.ogg)" \
    --add-password="Mot de passe" \
    --add-password="Confirmer le mot de passe" \
    --separator="|" 2>/dev/null)
annulzen
# récupération des valeurs
point=$(echo $CFPM | cut -d"|" -f1)
pass=$(echo $CFPM | cut -d"|" -f2)
pass1=$(echo $CFPM | cut -d"|" -f3)
while [ "$pass" !=  "$pass1" ] || [ -z "$pass" ]; do
zenity --width=500 --info --title="Mot de passe?" --text="Les mots de passe ne coïncident pas. Relance 
de la configuration du mot de passe"
CFPASS=$(zenity --width=500 --forms \
    --title="Configuration du mot de passe" \
    --text="Définition du mot de passe" \
    --add-password="Mot de passe" \
    --add-password="Confirmer le mot de passe" \
    --separator="|" 2>/dev/null)
annulzen
pass=$(echo $CFPASS | cut -d"|" -f1)
pass1=$(echo $CFPASS | cut -d"|" -f2)
done
echo "pm,$point
pass,$pass" > $FICHIERPM
zenity --width=500 --info --title="Point de diffusion configuré" --text="Le point de diffusion qui sera utilisé est $point.
Les auditeurs pourront vous écouter à l'adresse suivante: http://$SERVEURACAD/$point"
}

function configCS {
CARTES=$(aplay -l | grep ^carte)
aplay -l | grep ^carte > $FICHIERCS
# zenity --width=500 n'aime pas les espaces pour la fonction 'entry', donc suppression des espaces dans le nom des cartes
sed -i 's/ /_/g' $FICHIERCS
LISTECARTES=$(cat $FICHIERCS)
NBRECARTES=$(wc -l $FICHIERCS | cut -d" " -f1)
if [ "$NBRECARTES" -eq 1 ]; then
	zenity --width=500 --info --title="Carte son configurée" --text="La carte son qui sera utilisée pour les enregistrements et la diffusion est la suivante:
$LISTECARTES"
	nombre=$(echo $LISTECARTES | cut -d":" -f1 | tail -c2)
	nombre1=$(echo $LISTECARTES | cut -d":" -f2 | tail -c2)
	echo "$nombre,$nombre1" > $FICHIERCS
else
	CARTE=$(zenity --width=500 --entry --title="Choix de la carte son" --text="Sélectionner la carte son à utiliser" $LISTECARTES 2>/dev/null)
	annulzen
	nombre=$(echo $CARTE | cut -d":" -f1 | tail -c2)
	nombre1=$(echo $CARTE | cut -d":" -f2 | tail -c2)
	echo "$nombre,$nombre1" > $FICHIERCS
	CARTEOK=$(aplay -l | grep ^"carte $nombre" | grep "périphérique $nombre1")
	zenity --width=500 --info --title="Carte son configurée" --text="Au prochain enregistrement ou à la prochaine diffusion, la carte $CARTEOK sera utilisée."
fi
}

function verifIP {
if  [ -z "$IP" ]; then
	zenity --width=500 --info --title="Diffusion impossible" --text="Studiobox n'est pas connecté au réseau de 
l'établissement: il est donc impossible de procéder à une diffusion." 2>/dev/null
	exit
fi
}

function verifCS {
nombre=$(cat $FICHIERCS | cut -d"," -f1)
nombre1=$(cat $FICHIERCS | cut -d"," -f2)
DISPO=$(aplay -l | grep ^"carte $nombre" |  grep "périphérique $nombre1")
if [ -z "$DISPO" ] || [ -z "$nombre" ] ; then
        zenity --width=500 --info --title="Carte son indisponible" --text="Il semblerait que la carte son configurée soit indisponible.
Veuillez la reconfigurer (menu 'Outils WebRadio' > 'Configurer' > 'Choisir la carte son')" 2>/dev/null
	exit
fi
}

function verifPM {
TYPEPM=$(cat $FICHIERPM | grep ^pm | cut -d"," -f2 | cut -d"." -f2)
if [ "$TYPEPM" != ogg ] && [ "$TYPEPM" != mp3 ]; then
	zenity --width=500 --info --text="La diffusion sur internet n'est pas configurée.
(menu 'Outils WebRadio' > 'Configurer' > 'Configurer la diffusion sur internet')" 2>/dev/null
	exit
fi
}

function configQREC {
if [ -e $FICHIERQREC ]; then
	QRECDEF=$(cat $FICHIERQREC)
else
	QRECDEF='9'
fi
QREC=$(zenity --width=500 --scale --min-value=1 --max-value=9 --value=$QRECDEF --title="Qualité de l'enregistrement" --text="Choisissez la qualité de l'enregistrement (1 = très faible, 9 = excellente).")
annulzen
echo $QREC > $FICHIERQREC
}

function configQDIFF {
if [ -e $FICHIERQDIFF ]; then
	QDIFFDEF=$(cat $FICHIERQDIFF)
else
	QDIFFDEF='5'
fi
QDIFF=$(zenity --width=500 --scale --min-value=1 --max-value=9 --value=$QDIFFDEF --title="Qualité de la diffusion" --text="Choisissez la qualité de la diffusion (1 = très faible, 9 = excellente).")
annulzen
echo $QDIFF > $FICHIERQDIFF
}

function verifQREC {
CHIFFREVORBISREC=$(cat $FICHIERQREC 2>/dev/null)
if [ ! -z $CHIFFREVORBIS ]; then
	QUALITEVORBISREC="quality=0.$CHIFFREVORBISREC"
else
	QUALITEVORBISREC="quality=0.9"
fi
}

function verifQDIFF {
# en fonction du mode de diffusion et du format du point de montage, transformation du choix
# de la qualité de diffusion en valeur lisible par liquidsoap
if [ "$1" = local ] || [ "$1" = airtime ] ; then
	CHIFFREVORBISDIFF=$(cat $FICHIERQDIFF 2>/dev/null)
	if [ ! -z $CHIFFREVORBISDIFF ]; then
		QUALITEVORBISDIFF="quality=0.$CHIFFREVORBISDIFF"
	else
		QUALITEVORBISDIFF="quality=0.7"
	fi
else
	if [ "$TYPEPM" = ogg ]; then
		CHIFFREVORBISDIFF=$(cat $FICHIERQDIFF 2>/dev/null)
		if [ ! -z $CHIFFREVORBISDIFF ]; then
			QUALITEVORBISDIFF="quality=0.$CHIFFREVORBISDIFF"
		else
			QUALITEVORBISDIFF="quality=0.5"
		fi
	else
		CHIFFREMP3DIFF=$(cat $FICHIERQDIFF 2>/dev/null)
		if [ ! -z $CHIFFREMP3DIFF ]; then
			case $CHIFFREMP3DIFF in
				'1')
				QUALITEMP3DIFF='bitrate=48'
				;;
				'2')
				QUALITEMP3DIFF='bitrate=64'
				;;
				'3')
				QUALITEMP3DIFF='bitrate=96'
				;;
				'4')
				QUALITEMP3DIFF='bitrate=128'
				;;
				'5')
				QUALITEMP3DIFF='bitrate=160'
				;;
				'6')
				QUALITEMP3DIFF='bitrate=192'
				;;
				'7')
				QUALITEMP3DIFF='bitrate=224'
				;;
				'8')
				QUALITEMP3DIFF='bitrate=256'
				;;
				'9')
				QUALITEMP3DIFF='bitrate=320'
				;;
			esac
		else
			QUALITEMP3DIFF='bitrate=128'
		fi	
	fi
fi
}

function verifINT {
wget -q --tries=20 --timeout=10 http://www.google.com -O /tmp/google.idx &> /dev/null
if [ ! -s /tmp/google.idx ]
then
	zenity --width=500 --info --text="Vous n'êtes pas connecté à internet. La diffusion est impossible." 2>/dev/null
	exit
else
	rm /tmp/google.idx
fi
}

# amélioration possible sur toutes les fonctions de diffusion et d'enregistrement: effectuer un test 
# pour vérifier que la diffusion et / ou l'enregistrement se déroulent correctement

function choixCONFIG {
zenity --warning --width=500 --title="Avertissement" --text="Le menu suivant vous permettra d'effectuer différents réglages et de fixer différents paramètres, qui seront conservés si le mode persistance de la clé est activé.. 
Ces réglages ne sont donc normalement à faire qu'une fois. 
Les éventuels nouveaux réglages ecraseront les anciens.
" 
zenity --width="500" --question --title="Configuration" --text="Voulez-vous configurer la qualité de la diffusion (par défaut elle est égale à 5 sur une échelle de 0 à 9)?"
annulzen
bash .Scripts/diffrec-LS.bash configureQDIFF
zenity --width="500" --question --title="Configuration" --text="Voulez vous poursuivre avec la configuration de la qualité de l'enregistrement (par défaut elle est égale à 9 sur une échelle de 0 à 9)?"
annulzen
bash .Scripts/diffrec-LS.bash configureQREC
zenity --width="500" --question --title="Configuration" --text="Voulez vous poursuivre avec la configuration de la carte son?"
annulzen
bash .Scripts/diffrec-LS.bash configureCS
zenity --width="500" --question --title="Configuration" --text="Le point de diffusion actuellement configuré est $PMACAD. Voulez vous le modifier?"
annulzen
bash .Scripts/diffrec-LS.bash configurePM
zenity --warning --width="500" --text="La configuration est maintenant terminée. Si vous avez activé la persistance sur votre clé ces réglages seront conservés pour ses prochaines utilisations.
Cependant, si vous changez d'ordinateur pour la diffusion, ou si vous modifiez les ports USB sur lesquels sont branchés la clé et/ou la table de mixage, vous devriez procéder à nouveau à la configuration."
}


function choixDIFF {
# Enregistrer ou diffuser?
zenity --warning --width=500 --title="Avertissement" --text="Avant de vous lancer, assurez vous que votre point de diffusion n'est pas occupé. Regardez dans la la liste de processus qui s'affichent sur la droite de l'écran s'il existe un process \"liquidsoap\". 
Si c'est le cas, ouvrez un terminal à partir du menu principal et entrez la commande \"killall liquidsoap\". 
Verifiez que le process a bien disparu. 
C'est prêt. " 
choix=`zenity --width=500 --height=220 --list --title="Quel type d'opération voulez vous réaliser?" --text="Faites un choix parmi les options proposées puis validez." --column='Que voulez-vous faire ?' \
        "1-Enregistrer une émission" "2-Diffuser en direct sur le réseau local" "3-Diffuser en direct sur le réseau local et enregistrer" "4-Diffuser en direct sur Internet" "5-Diffuser en direct sur Internet et enregistrer"`
annulzen

choix2=${choix:0:1}
echo $choix2
if [ $choix2 = 1 ];then lxterminal -e '.Scripts/diffrec-LS.bash rec'
elif [ $choix2 = 2 ];then lxterminal -e '.Scripts/diffrec-LS.bash local'
elif [ $choix2 = 3 ];then lxterminal -e '.Scripts/diffrec-LS.bash localrec'
elif [ $choix2 = 4 ];then lxterminal -e '.Scripts/diffrec-LS.bash internet'
elif [ $choix2 = 5 ];then lxterminal -e '.Scripts/diffrec-LS.bash internetrec'
fi
}

function choixPROG {
#Explications
zenity --warning --width=500 --title="Avertissement" --text="Il est possible de programmer la diffusion d'émissions en différé de deux façons :
1- Programmer une émission à une date et une heure donnée, pour une diffusion unique ou récurrente (quotidienne, hebdomadaire, mensuelle).
2- Programmer la diffusion d'une playlist composée de plusieures émissions, éventuellement entrecoupées de musique et de jingles. La playlist peut être diffusée en boucle 24h sur 24 ou sur des plages horaires définies à l'avance." 
# Programmation d'une émission unique ou d'une playlist ?
choix=`zenity --width=500 --height=200 --list --title="Quel type de programmation voulez vous mettre en place?" --text="Faites un choix parmi les options proposées puis validez." --column='Que voulez-vous faire ?' \
        "1-Programmer une émission" "2-Programmer une \"playlist\"" "3-Réinitialiser la programmation"`
annulzen
choix2=${choix:0:1}
if [ $choix2 = 1 ];then bash .Scripts/diffrec-LS.bash configurePROG
elif [ $choix2 = 2 ];then bash .Scripts/diffrec-LS.bash configurePLAYLIST
elif [ $choix2 = 3 ];then bash .Scripts/diffrec-LS.bash supprimePROG
fi
}

function configPLAY {
#Explications
zenity --question --width=500 --title="Avertissement" --text="
Les fichiers constituant la playlist doivent être déposés - dans l'ordre souhaité pour la diffusion - dans les dossiers suivants :

1- Les fichiers d'émissions dans Documents/playlist/emissions
2- Les fichiers musicaux dans Documents/playlist/musiques
3- les jingles, virgules et autres dans Documents/playlist/divers

Les noms de fichiers ne doivent pas comporter d'espaces, ni de caractères diacritiques (accents, cédilles,...), ni de caractères particuliers à l'exception des espaces et du point.

Votre playlist est-elle prête ?"
annulzen
# Choix du type
choix=`zenity --list --width=500 --title="Type de diffusion" --text="Faites un choix parmi les options proposées puis validez" --column='Que voulez-vous faire ?' \
        "1-Boucle permanente" "2-Boucle sur une plage horaire donnée"`
annulzen



}


function difflocal {
zenity --width=500 --question --title="Diffusion en direct" --text="$TEXTEZENDIFFLOCAL
$TEXTEMONITOR" 2>/dev/null
if [ $? = 0 ]; then
	liquidsoap "output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMLOCAL\",host=\"$SERVEURLOCAL\", port=$PORTICECAST , password=\"$PASSLOCAL\",input.alsa(device=\"hw:$nombre,$nombre1\"))" &
	vlc http://$SERVEURLOCAL:$PORTICECAST/$PMLOCAL $VLCVIEW 2>/dev/null
else
	liquidsoap "output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMLOCAL\",host=\"$SERVEURLOCAL\", port=$PORTICECAST , password=\"$PASSLOCAL\",input.alsa(device=\"hw:$nombre,$nombre1\"))"
fi
}

function difflocalrec {
zenity --width=500 --question --title="Diffusion et enregistrement" --text="$TEXTEZENDIFFLOCAL
$TEXTEZENREC
$TEXTEMONITOR" 2>/dev/null
if [ $? = 0 ]; then
	liquidsoap "s=output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMLOCAL\",host=\"$SERVEURLOCAL\", port=$PORTICECAST , password=\"$PASSLOCAL\",input.alsa(device=\"hw:$nombre,$nombre1\")) output.file(%vorbis($QUALITEVORBISREC),\"~/$REPREC/$FICHIERREC\",s)" &
	vlc http://$SERVEURLOCAL:$PORTICECAST/$PMLOCAL $VLCVIEW 2>/dev/null
else
	liquidsoap "s=output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMLOCAL\",host=\"$SERVEURLOCAL\", port=$PORTICECAST , password=\"$PASSLOCAL\",input.alsa(device=\"hw:$nombre,$nombre1\")) output.file(%vorbis($QUALITEVORBISREC),\"~/$REPREC/$FICHIERREC\",s)"
fi
}

function localrec {
zenity --width=500 --question --title="Enregistrement" --text="$TEXTEZENREC
$TEXTEMONITOR" 2>/dev/null
if [ $? = 0 ]; then
	liquidsoap "s=output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMMONITOR\",host=\"$SERVEURLOCAL\", port=$PORTICECAST , password=\"$PASSMONITOR\",input.alsa(device=\"hw:$nombre,$nombre1\")) output.file(%vorbis($QUALITEVORBISREC),\"~/$REPREC/$FICHIERREC\",s)" &
	vlc http://$SERVEURLOCAL:$PORTICECAST/$PMMONITOR $VLCVIEW 2>/dev/null
else
	liquidsoap "output.file(%vorbis($QUALITEVORBISREC),\"~/$REPREC/$FICHIERREC\",input.alsa(device=\"hw:$nombre,$nombre1\"))"
fi
}

function diffinternet {
zenity --width=500 --question --title="Diffusion en direct sur internet" --text="$TEXTEZENDIFFINT
$TEXTEMONITOR" 2>/dev/null
if [ $? = 0 ]; then
	if [ "$TYPEPM" = "ogg" ]; then
		liquidsoap "output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\"))" &
		vlc http://$SERVEURACAD/$PMACAD $VLCVIEW 2>/dev/null
	else
		liquidsoap "output.icecast(%mp3($QUALITEMP3DIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\"))" &
		vlc http://$SERVEURACAD/$PMACAD $VLCVIEW 2>/dev/null
	fi
else
	if [ "$TYPEPM" = "ogg" ]; then
		liquidsoap "output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\"))"
	else
		liquidsoap "output.icecast(%mp3($QUALITEMP3DIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\"))"
	fi

fi
}

function diffinternetrec {
zenity --width=500 --question --title="Diffusion en direct sur internet" --text="$TEXTEZENDIFFINT
$TEXTEZENREC
$TEXTEMONITOR" 2>/dev/null 
if [ $? = 0 ]; then
	if [ "$TYPEPM" = "ogg" ]; then
		liquidsoap "s=output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\")) output.file(%vorbis($QUALITEVORBISREC),\"~/$REPREC/$FICHIERREC\",s)" &
		vlc http://$SERVEURACAD/$PMACAD $VLCVIEW 2>/dev/null
	else
		liquidsoap "s=output.icecast(%mp3($QUALITEMP3DIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\")) output.file(%vorbis($QUALITEVORBISREC),\"~/$REPREC/$FICHIERREC\",s)" &
		vlc http://$SERVEURACAD/$PMACAD $VLCVIEW 2>/dev/null
	fi
else
	if [ "$TYPEPM" = "ogg" ]; then
		liquidsoap "s=output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\")) output.file(%vorbis($QUALITEVORBISREC),\"~/$REPREC/$FICHIERREC\",s)"
	else
		liquidsoap "s=output.icecast(%mp3($QUALITEMP3DIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\")) output.file(%vorbis($QUALITEVORBISREC),\"~/$REPREC/$FICHIERREC\",s)"
	fi
fi
}

function diffairtime {
	zenity --width=500 --question --title="Lancement de la diffusion" --text="$TEXTEZENAIR
$TEXTEMONITOR" 2>/dev/null
if [ $? = 0 ]; then
	liquidsoap "s=output.icecast(%vorbis($QUALITEVORBISREC), mount=\"$PMAIRTIME\",host=\"$SERVEURLOCAL\",port=$PORTAIRTIME,user=\"$USERAIRTIME\",password=\"$PASSAIRTIME\",input.alsa(device=\"hw:$nombre,$nombre1\")) output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMMONITOR\",host=\"$SERVEURLOCAL\", port=$PORTICECAST , password=\"$PASSMONITOR\",s)"  &
	sleep 2
	vlc http://$SERVEURLOCAL:$PORTICECAST/$PMMONITOR $VLCVIEW 2>/dev/null
else
	liquidsoap "output.icecast(%vorbis($QUALITEVORBISREC), mount=\"$PMAIRTIME\",host=\"$SERVEURLOCAL\",port=$PORTAIRTIME,user=\"$USERAIRTIME\",password=\"$PASSAIRTIME\",input.alsa(device=\"hw:$nombre,$nombre1\"))"
fi
}

function diffairtimerec {
	zenity --width=500 --question --title="Lancement de la diffusion et de l'enregistrement" --text="$TEXTEZENAIR
$TEXTEZENREC
$TEXTEMONITOR" 2>/dev/null 
if [ $? = 0 ]; then
	liquidsoap "s=output.icecast(%vorbis($QUALITEVORBISREC), mount=\"$PMAIRTIME\",host=\"$SERVEURLOCAL\",port=$PORTAIRTIME,user=\"$USERAIRTIME\",password=\"$PASSAIRTIME\",input.alsa(device=\"hw:$nombre,$nombre1\")) output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMMONITOR\",host=\"$SERVEURLOCAL\", port=$PORTICECAST , password=\"$PASSMONITOR\",s) output.file(%vorbis($QUALITEVORBISREC),\"~/$REPREC/$FICHIERREC\",s)" &
	sleep 2
	vlc http://$SERVEURLOCAL:$PORTICECAST/$PMMONITOR $VLCVIEW 2>/dev/null
else
	liquidsoap "s=output.icecast(%vorbis($QUALITEVORBISREC), mount=\"$PMAIRTIME\",host=\"$SERVEURLOCAL\",port=$PORTAIRTIME,user=\"$USERAIRTIME\",password=\"$PASSAIRTIME\",input.alsa(device=\"hw:$nombre,$nombre1\")) output.file(%vorbis($QUALITEVORBISREC),\"~/$REPREC/$FICHIERREC\",s)"
fi
}

function diffprog {
zenity --width=500 --question --title="Diffusion programmée" --text="$TEXTEZENDIFFINT
$TEXTEMONITOR" 2>/dev/null
if [ $? = 0 ]; then
	if [ "$TYPEPM" = "ogg" ]; then
		liquidsoap "output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\"))" &
		vlc http://$SERVEURACAD/$PMACAD $VLCVIEW 2>/dev/null
	else
		liquidsoap "output.icecast(%mp3($QUALITEMP3DIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\"))" &
		vlc http://$SERVEURACAD/$PMACAD $VLCVIEW 2>/dev/null
	fi
else
	if [ "$TYPEPM" = "ogg" ]; then
		liquidsoap "output.icecast(%vorbis($QUALITEVORBISDIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\"))"
	else
		liquidsoap "output.icecast(%mp3($QUALITEMP3DIFF), mount=\"$PMACAD\",host=\"$SERVEURACAD\", port=$PORTICECAST , password=\"$PASSACAD\",input.alsa(device=\"hw:$nombre,$nombre1\"))"
	fi

fi
}


function configPG {
zenity --warning --width=500 --title="Avertissement" --text="Les émissions enregistrées à l'aide de Studiobox se trouvent dans le dossier 'Enregistrements'. Si vous ajoutez des fichiers, le nom à utiliser pour leur diffusion doit être composé de lettres non accentuées, de chiffres et des caractères '-' et '_', à l'exclusion de tout autre signe. Les espaces dans les noms de fichiers sont à proscrire absolument.
Pour que la diffusion fonctionne, la clé Studiobox doit bien-sûr être en cours d'utilisation. Si la persistance est activée, vous pouvez débrancher la clé après programmation et la remettre en service plus tard."

#choix du programme à diffuser.
emission=`yad --center --text="Choisissez le programme à diffuser" --title="Choix du programme à diffuser" --width=500 --file-selection`
annulzen

#Type de diffusion
type=`zenity --width=500 --height=200 --list --title="Quel type de diffusion voulez vous programmer?" --text="Faites un choiX parmi les options proposées puis validez." --column='Type de programmation' \
        Unique Quotidienne Hebdomadaire Mensuelle`
annulzen

# Diffusion unique
if [ $type = Unique ]; then
PROG=$(zenity --width=500 --forms \
    --title="Configuration de la diffusion" \
    --text="Choisissez la date et l'heure de diffusion de votre émission" \
    --add-calendar --date-format "%Y%m%d"="Jour" \
    --add-entry="Entrez l'heure au format 'hh:mm'" \
    --separator="|" 2>/dev/null)
annulzen
day=$(echo $PROG | cut -d"|" -f1)
time=$(echo $PROG | cut -d"|" -f2)
day2=$(echo "${day////.}")
zenity --width=500 --question --text="Confirmez vous la programmation de l'émission $emission à $time le $day."
annulzen
if [ "$TYPEPM" = "ogg" ]; then
echo "killall liquidsoap;/usr/bin/liquidsoap 'output.icecast(%vorbis($QUALITEVORBISDIFF), host = \"$SERVEURACAD\", port = 8000, password = \"$PASSACAD\", mount =\"$PMACAD\", fallible = true, once(single(\"$emission\")))' --quiet" | at $time $day2
else
echo "killall liquidsoap;/usr/bin/liquidsoap 'output.icecast(%mp3($QUALITEMP3DIFF), host = \"$SERVEURACAD\", port = 8000, password = \"$PASSACAD\", mount =\"$PMACAD\", fallible = true, once(single(\"$emission\")))' --quiet" | at $time $day2		
fi
zenity --width=500 --info --text="Émission programmée le $day à $time"

# Diffusion quotidienne
elif [ $type = Quotidienne ]; then
time=$(zenity --width=500 --title="Configuration de la diffusion" --text="Entrez l'heure au format 'hh:mm'" --entry)
heure=$(echo "${time:0:2}")
minute=$(echo "${time:3:4}")
zenity --width=500 --question --text="Confirmez-vous la programmation de l'émission $emission chaque jour à $heure h $minute?"
#Test sur l'existence de la crontab et création si besoin
	table=$(sudo ls /var/spool/cron/crontabs/)
	if [ -z $table ]; then
	sudo touch /var/spool/cron/crontabs/studiobox
	sudo chown studiobox:crontab /var/spool/cron/crontabs/studiobox
	fi
# Mise en place de l'entrée crontab
if [ "$TYPEPM" = "ogg" ]; then
(crontab -l;echo "$minute $heure * * * killall liquidsoap;/usr/bin/liquidsoap 'output.icecast(\%vorbis($QUALITEVORBISDIFF), host = \"$SERVEURACAD\", port = 8000, password = \"$PASSACAD\", mount =\"$PMACAD\", fallible = true, once(single(\"$emission\")))' --quiet") | crontab -
else
(crontab -l;echo "$minute $heure * * * killall liquidsoap;/usr/bin/liquidsoap 'output.icecast(\%mp3($QUALITEMP3DIFF), host = \"$SERVEURACAD\", port = 8000, password = \"$PASSACAD\", mount =\"$PMACAD\", fallible = true, once(single(\"$emission\")))' --quiet") | crontab -
fi
zenity --width=500 --info --text="Émission programmée tous les jours à $heure h $minute"

# Diffusion hebdomadaire
elif [ $type = Hebdomadaire ]; then
jour=$(zenity --width=500 --title="Configuration de la diffusion" --text "Jour de diffusion" --entry 1-Lundi 2-Mardi 3-Mercredi 4-Jeudi 5-Vendredi 6-Samedi 7-Dimanche)
annulzen
dow=$(echo "${jour:0:1}")
day=$(echo "${jour:2}")
time=$(zenity --width=500 --title="Configuration de la diffusion" --text="Entrez l'heure au format 'hh:mm'" --entry)
annulzen
heure=$(echo "${time:0:2}")
minute=$(echo "${time:3:4}")
zenity --width=500 --question --text="Confirmez-vous la programmation de l'émission $emission chaque $day à $heure h $minute?"
annulzen
#Test sur l'existence de la crontab et création si besoin
	table=$(sudo ls /var/spool/cron/crontabs/)
	if [ -z $table ]; then
	sudo touch /var/spool/cron/crontabs/studiobox
	sudo chown studiobox:crontab /var/spool/cron/crontabs/studiobox
	fi
# Mise en place de l'entrée crontab
if [ "$TYPEPM" = "ogg" ]; then
(crontab -l;echo "$minute $heure * * $dow killall liquidsoap;/usr/bin/liquidsoap 'output.icecast(\%vorbis($QUALITEVORBISDIFF), host = \"$SERVEURACAD\", port = 8000, password = \"$PASSACAD\", mount =\"$PMACAD\", fallible = true, once(single(\"$emission\")))' --quiet") | crontab -
else
(crontab -l;echo "$minute $heure * * $dow killall liquidsoap;/usr/bin/liquidsoap 'output.icecast(\%mp3($QUALITEMP3DIFF), host = \"$SERVEURACAD\", port = 8000, password = \"$PASSACAD\", mount =\"$PMACAD\", fallible = true, once(single(\"$emission\")))' --quiet") | crontab -
fi
zenity --width=500 --info --text="Émission programmée chaque $day à $heure h $minute"

# Diffusion Mensuelle
else
jour=$(zenity --width=500 --title="Configuration de la diffusion" --text "Jour de diffusion mensuelle (de 1 à 31)" --entry)
annulzen
time=$(zenity --width=500 --title="Configuration de la diffusion" --text="Entrez l'heure au format 'hh:mm'" --entry)
annulzen
heure=$(echo "${time:0:2}")
minute=$(echo "${time:3:4}")
zenity --width=500 --question --text="Confirmez-vous la programmation de l'émission $emission le $jour de chaque mois à $heure h $minute?"
annulzen
#Test sur l'existence de la crontab et création si besoin
	table=$(sudo ls /var/spool/cron/crontabs/)
	if [ -z $table ]; then
	sudo touch /var/spool/cron/crontabs/studiobox
	sudo chown studiobox:crontab /var/spool/cron/crontabs/studiobox
	fi
# Mise en place de l'entrée crontab
if [ "$TYPEPM" = "ogg" ]; then
(crontab -l;echo "$minute $heure $jour * * killall liquidsoap;/usr/bin/liquidsoap 'output.icecast(\%vorbis($QUALITEVORBISDIFF), host = \"$SERVEURACAD\", port = 8000, password = \"$PASSACAD\", mount =\"$PMACAD\", fallible = true, once(single(\"$emission\")))' --quiet") | crontab -
else
(crontab -l;echo "$minute $heure $jour * * killall liquidsoap;/usr/bin/liquidsoap 'output.icecast(\%mp3($QUALITEMP3DIFF), host = \"$SERVEURACAD\", port = 8000, password = \"$PASSACAD\", mount =\"$PMACAD\", fallible = true, once(single(\"$emission\")))' --quiet") | crontab -
fi
zenity --width=500 --info --text="Émission programmée le $jour de chaque mois à $heure h $minute"

fi
}

#Suppression de la programmation par crontab et at
function supprPG {
zenity --question --width="500" --text="Voulez-vous effacer l'ensemble de la programmation ou n'en supprimer qu'une entrée ?" --ok-label="Tout" --cancel-label="Une partie"
	if [ $? = 0 ];then
crontab -r
for i in `atq | awk '{print $1}'`;
do
atrm $i
done
zenity --question --width="500" --text="L'ensemble de la programmation à été effacé. Voulez-vous reprendre la programmation d'une emission?"
		if [ $? = 0 ] ; then
		choixPROG
		fi
	else
	zenity --info --width="500" --text="C'est plus compliqué, voila ce qu'il faut faire ....

- Pour effacer une programmation \"unique\", ouvrez un terminal et entrez la commande \"atq\". Cette commande liste les différentes tâches programmées, en commençant par un numéro. Notez le numéro de la tâche que vous voulez supprimer, et entrez dans le terminal la commande \"atrm\" suivi d'un espace et du numéro de la tâche à supprimer. Vérifiez sa disparition avec la commande \"atq\".

- Pour effacer une programmation \"récurrente\", ouvrez un terminal et entrez la commande \"crontab -e\". Cette commande liste les différentes tâches programmées, en commençant par 5 champs séparés par des espaces : minute (de 0 à 59) heure (de 0 à 23) jour (de 1 à 31) mois (de 1 à 12) et jour de la semaine (de 1 pour lundi à 7 pour dimanche). 
Si le champ contient une étoile, cela signifie \"toutes les occurrences possibles\". Par exemple une étoile dans le deuxième champ signifie \"À toutes les heures\".
Le sixième champ enfin indique la tâche à lancer. Vous lirez en toute fin de ligne le nom du fichier concerné. 
En effaçant une ligne, vous supprimez la tâche en question.
Enregistrez la suppression en appuyant sur CTRL+O et en validant et quittez l'éditeur en appuyant sur CTRL+X.
Vérifiez la suppression en entrant à nouveau \"crontab -e\" et quittez l'éditeur en appuyant sur CTRL+X."
	fi
}


case $1 in
	'choix')
		choixDIFF
	;;
	'choix2')
		choixPROG
	;;
	'configureCS')
		configCS
	;;
	'configurePM')
		configPM
	;;
	'configureQDIFF')
		configQDIFF
	;;
	'configureQREC')
		configQREC
	;;
	'configureCONFIG')
		choixCONFIG
	;;
	'rec')
		verifCS
		verifQREC
		localrec
	;;
	'local')
		verifIP
		verifCS
		verifQDIFF $1
		difflocal
	;;
	'localrec')
		verifIP
		verifCS
		verifQDIFF $1
		verifQREC
		difflocalrec
	;;
	'internet')
		verifINT
		verifPM
		verifCS
		verifQDIFF $1
		diffinternet
	;;
	'internetrec')
		verifINT
		verifPM
		verifCS
		verifQDIFF $1
		verifQREC
		diffinternetrec
	;;
	'configurePROG')
		verifINT
		verifPM
		verifQDIFF $1
		configPG
	;;
	'configurePLAYLIST')
		verifINT
		verifPM
		verifQDIFF $1
		configPLAY
	;;
	'supprimePROG')
		supprPG
	;;	
esac
