#!/bin/bash
chaine=$(cat /home/studiobox/.conkyrc)
enp=$(/sbin/ifconfig -a | awk '{print $1}' | grep enp | awk 'sub(".$", "")')
echo $enp
wlp=$(/sbin/ifconfig -a | awk '{print $1}' | grep wlp | awk 'sub(".$", "")')
echo $wlp
if [[ "$chaine" =~ "InterfaceFilaire" ]]
then
sed -i -e "s/InterfaceFilaire/$enp/g" /home/studiobox/.conkyrc
echo "fait"
fi

if [[ "$chaine" =~ "InterfaceWifi" ]]
then
sed -i -e "s/InterfaceWifi/$wlp/g" /home/studiobox/.conkyrc
echo "fait"
fi
