#!/bin/bash

PERSISTENCE=$(ls /lib/live/mount/persistence/sd* | grep persistence.conf)


#PREMIER CAS : la partition persistante n'existe pas et le fichier menupersistence.xml existe (premier démarrage).
if [ -z "$PERSISTENCE" ] && [ -f "$HOME/.config/openbox/menupersistence.xml" ]; then
	mv $HOME/.config/openbox/menupersistence.xml $HOME/.config/openbox/menu.xml
	openbox --reconfigure

# renvoi au TROISIEME CAS
	bash .config/openbox/autostart.sh



# DEUXIEME CAS : La partition persistence existe et le fichier menupersistence.xml existe (premier démarrage après mise en place de la persistance).
elif [ -n "$PERSISTENCE" ] && [ -f "$HOME/.config/openbox/menupersistence.xml" ]; then
	zenity --width=400 --info --title="Bienvenue sur Studiobox5" --text="Bienvenue sur le système d'exploitation Studiobox5.
Pour mémoire:
- votre nom d'utilisateur est 'studiobox'
- votre mot de passe est 'live'"
	mv -f $HOME/.config/openbox/menupersistence.xml $HOME/.config/openbox/menu.xml
	openbox --reconfigure
	sed -i '/menupersistence/d' .config/openbox/autostart.sh


#QUATRIEME CAS : la partition persitence existe et le fichier menupersistence.xml n'existe pas. Normalement pas possible. On renvoie au deuxième cas.
elif [ -n "$PERSISTENCE" ] && [ ! -f "$HOME/.config/openbox/menupersistence.xml" ]; then
	cp $HOME/.config/openbox/menu.xml $HOME/.config/openbox/menupersistence.xml
	openbox --reconfigure
	sed -i '/menupersistence/d' .config/openbox/autostart.sh

#TROISIEME CAS : La partition persistence n'existe pas et le fichier menupersitence.xml n'existe pas (suite du premier cas) : on crée la partitiion.
else
	zenity --width=500 --question --title="Bienvenue sur Studiobox v.5" --text="Studiobox fonctionne actuellement en mode 'non-persistant'.
Cela signifie que tous les réglages que vous effectuez et que tous les fichiers que vous créez sont effacés à chaque redémarrage.
Souhaitez-vous lancer l'outil de création de la persistance maintenant?"
	if [ $? = 0 ]; then
		USB_DISK_ID=$(ls /dev/disk/by-id/ | grep ^usb | grep ':0'$)
		USBDISKSLIST="$HOME/.Scripts/config/usbdiskslist"
		touch $USBDISKSLIST
		rm $USBDSIKSLIST
		echo $USB_DISK_ID > $USBDISKSLIST
	else
		exit
	fi
	NOMBREDISQUES=$(cat $USBDISKSLIST | wc -l)
	if [ $NOMBREDISQUES -eq 1 ]; then
		USB_DISK_DEV=$(readlink -e /dev/disk/by-id/$USB_DISK_ID)
		SIZE=$(sudo parted -l | grep $USB_DISK_DEV | cut -d":" -f2)
		USBNAME=$(echo $USB_DISK_DEV | cut -d'/' -f3)
		zenity --width=500 --question --title="Mise en place de la persistance" --text="La clé $USB_DISK_DEV (taille:$SIZE) a été repérée comme clé usb Studiobox.
Mettre en place la persistance sur cette clé?" 2>/dev/null
		if [ $? = 0 ]; then
		lxterminal -l -e "sudo bash /home/studiobox/.Scripts/persistence.sh $USB_DISK_DEV" | tee >(zenity --no-cancel --progress --pulsate --auto-close --title="Mise en place de la persistance" --text="Cela peut prendre plusieurs minutes, selon la taille de la clé USB... merci de patienter" 2>/dev/null)
		else
			exit
		fi
	else
		CHOIXDISQUE=$(zenity --entry --title="Choisir la clé USB sur laquelle studiobox est installé" --text="Choisir la clé USB dans le menu déroulant" $USB_DISK_ID 2>/dev/null)
		USB_DISK_DEV=$(readlink -e /dev/disk/by-id/$CHOIXDISQUE)
		lxterminal -l -e "sudo bash /home/studiobox/.Scripts/persistence.sh $USB_DISK_DEV" | tee >(zenity --no-cancel --progress --pulsate --autoclose --title="Mise en place de la persistance" --text="Studiobox met en place la persistance, merci de patienter" 2>/dev/null)
	fi
	zenity --width=500 --question --title="Persistance mise place" --text="La persistance sera activée au prochain redémarrage.
Souhaitez-vous redémarrer dès maintenant?" 2>/dev/null
	if [ $? = 0 ]; then
		sudo reboot
	else
		exit
	fi
fi
